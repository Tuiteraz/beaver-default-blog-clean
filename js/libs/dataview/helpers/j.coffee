define [
  'jvm-console'
  'libs/dataview/jvm-templates'
  'libs/dataview/jvm-helpers'
  'underscore'
],(jconsole,tmpl,hlprs) ->
  window.j =
    html:
      # make html-safe from sValue's html
      # sValue - html text
      # +2013.4.30 tuiteraz
      encode: (sValue) ->
        # create a in-memory div, set it's inner text(which jQuery automatically encodes)
        # then grab the encoded contents back out.  The div never exists on the page.
        $('<div/>').text(sValue).html();

      # decode html-safe text to html
      # sValue - html-safe text
      # +2013.4.30 tuiteraz
      decode: (sValue) ->
        $('<div/>').html(sValue).text();

    object:
      attr:

        # получить значение атрибута. Если Attr - строка, то просто вернуть строку.
        # Если объект, то вернуть значение атрибута объекта по его sListViewAttrCode
        # +2013.3.23 tuiteraz
        # *2013.3.24 tuiteraz: +hElement
        #   hElement.fnValue called when _.isFunction hElement.fnValue
        # *2013.3.30 tuiteraz: +bCleanValue - без выполнения функции и иконок вместо true|false
        value: (Attr, hElement=null, bCleanValue=true) ->

          fnValByListViewAttr =  ->
            if _.isObject 'Attr'
              sListViewAttrCode = Attr.hObjectAttributes.sListViewAttrCode
              return Attr.hAttributes[sListViewAttrCode]
            else
              return Attr


          if _.isObject Attr # если Attr - это объект
            if _.isNull hElement
              return fnValByListViewAttr()
            else if defined hElement.fnValue
              if _.isFunction hElement.fnValue
                return hElement.fnValue Attr
              else
                return fnValByListViewAttr()
          else if !_.isNull hElement # если не пустой параметр атрибутов Attr
#            if defined hElement.fnValue
#              if _.isFunction hElement.fnValue # если указана функция вывода значения
#                if !_.isNull Attr
#                  return hElement.fnValue Attr
#                else
#                  return fnValByListViewAttr()
#              else
#              return fnValByListViewAttr()
#            else if _.isBoolean Attr
            if _.isBoolean(Attr) && !bCleanValue
              sIconName = "check"
              sIconName += if Attr then '' else '-empty'
              return "<i class='icon-#{sIconName}' />"
            else
              return fnValByListViewAttr()
          else if _.isBoolean(Attr) && !bCleanValue
            sIconName = "check"
            sIconName += if Attr then '' else '-empty'
            return "<i class='icon-#{sIconName}' />"
          else
            return Attr

    # заполним html форму данными полученными с сервера
    # hElements - ссылка на узел типа CONFMAN.tmpl.conf.consts.form.elements
    #  подразумевается, что все id в elements уникальны для текущего document.body
    # hAttrData - хэш серверных данных монго в виде: название атрибута => значение
    # +2012.11.10 Tuiteraz
    # *2012.12.12 Tuiteraz
    # *2013.1.10 Tuiteraz
    # *2013.3.4 Tuiteraz: если в параметрах не указан жесткий список опций то используем опции из html
    # *2013.3.7 Tuiteraz: call update_chosen_select_For on setting select value
    # *2013.3.20 Tuiteraz: * назначать значение элементу только если есть данные для него
    # *2013.3.22 Tuiteraz: moved from dataview/jvm-helpers to dataview/j
    #   + sId - если указана строка, то использовать селектор вида "sId hAttrs.sIdClass"
    # *2013.3.23 tuiteraz: Обработка вывода референсного атрибута, например refUser, который здесь представлен всемисвоими атрибутами
    # *2013.3.30 tuiteraz: выводить пустое значение если значения для атрибута не передано
    set_elements_data: (hElements,hAttrData, sId = null) ->
      jconsole.enable_log "set_elements_data"
      for sName, hAttrs of hElements
        sSelector = if _.isNull sId then "##{hAttrs.id}" else "##{sId} .#{hAttrs.sIdClass}"
        jObj = $(sSelector)
        if jObj.length != 0
          sAttrValue = ''
          sAttrValue = j.object.attr.value(hAttrData[sName],hAttrs) if defined hAttrData[sName]

          if hAttrs.sInputStyle == FRM.input.style.input
            jObj.val sAttrValue
          else if hAttrs.sInputStyle == FRM.input.style.text
            jObj.val sAttrValue
          else if hAttrs.sInputStyle == FRM.input.style.checkbox
            if defined hAttrData[sName]
              jObj[0].checked = sAttrValue
            else
              jObj[0].checked = false
          else if hAttrs.sInputStyle == FRM.input.style.select
            if defined hAttrs.hOptions
              jObj.attr 'value', hAttrs.hOptions[sAttrValue] if defined hAttrData[sName]
            else if $("#{sSelector} option").length > 0
              sCustomOptSelector = "#{sSelector} option[data-opt-link='#{sAttrValue}']"
              if $(sCustomOptSelector).length == 1
                jObj.attr 'value', $(sCustomOptSelector).attr 'value' if defined hAttrData[sName]
              else
                jObj.attr 'value', sAttrValue
            else
              jObj.attr 'value', sAttrValue if defined hAttrData[sName]
            update_chosen_select_for jObj
          else if (hAttrs.sInputStyle == FRM.input.style.radio_v) or (hAttrs.sInputStyle == FRM.input.style.radio_h)
            # сначала удалим все установки существующие
            if defined hAttrData[sName]
              sSelector = if _.isNull sId then "input[name='#{hAttrs.id}']" else "##{sId} input[name='#{hAttrs.sIdClass}']"
              $(sSelector).removeAttr 'checked'
              jObj = $("#{sSelector}[data-opt-code='#{sAttrValue}']")
              jObj.attr 'checked','checked'


    # проверка заполненности данных формы по флагам из настроек атрибута
    # которые копируются в хэш вместе с данными, чтобы не зависеть от узла настроек
    # в котором оригинально были указаны
    # формат:
    # hElemsData[sName]=
    #    Value
    #    sLabel
    #    sFullName
    #    sDataType
    #    bDataRequired
    # +2012.11.10 Tuiteraz
    # *2012.12.12 Tuiteraz: refactoring
    # *2012.12.27 Tuiteraz: refactoring
    # *2013.1.9 Tuiteraz: реальной валидации пока не производится в клиенте
    # *2013.3.21Tuiteraz: moved to j
    elements_data_is_valid: (hElemsData, sSection='') ->
      bRes = true
      aElemsErrors = []
      for sAttrName,hAttrs of hElemsData
        if ( hAttrs.bDataRequired == true ) and (hAttrs.Value.length == 0) and (sAttrName != 'id')
          bRes = false if bRes
          aElemsErrors.push {
                            sType: 'warn'
                            sTitle: hAttrs.sFullName
                            sMessage: "Не заполнен обязательный реквизит [#{hAttrs.sFullName}]"
                            }

      return if bRes then bRes else aElemsErrors

    # получить данные из html по хэшу элементов, по которому он формировался
    # параметром передается узел ветки CONFMAN
    # +2012.11.10 Tuiteraz
    # *2012.12.12 Tuiteraz: refactoring
    # *2013.3.1 Tuiteraz: refactoring -> get_elements_data()
    # *2013.3.21 Tuiteraz: moved to j
    get_elements_data: (hElements) ->
      hElemsData = {}
      hElemsData[sName]= j.frm.element.get_data(hAttrs) for sName, hAttrs of hElements
      return hElemsData


    # parse hash hData{ sCode:{Value:'value'}} -> hData{sCode:'value'}
    # parse hash hData{ refUser:{ email: {Value:'value'} } } -> hData{ refUser: {email:'value'} }
    # при этом разделение хэшей простого атрибута и ссылки происходит (ВНИМАНИЕ!) по атрибуту Value
    # если у хэша нет атрибута Value - то это будет трактоваться как ссылка на другой объект и будет выполнен рекурсивный вызов
    # глубина рекурсии не ограничена
    # +2013.3.20 tuiteraz
    parse_values_from_attr_hash: (hData)->
      hResData = {} # а вот это уже данные атрибутов сохраняемого объекта
      aKeys = _.keys hData
      for sName in aKeys
        if !_.isUndefined hData[sName].Value
          hResData[sName]=hData[sName].Value
        else if defined sName
          hResData[sName]= j.parse_values_from_attr_hash hData[sName]

      return hResData

    # устанавливает или получает текущий CSRF для страницы
    # +2013.3.18 tuiteraz
    CSRF: (hData=null) ->
      if defined hData
        $("meta[name='csrf-param']").attr 'content', hData.sParam
        $("meta[name='csrf-token']").attr 'content', hData.sToken
      else
        {
          sParam: $("meta[name='csrf-param']").attr ('content')
          sToken: $("meta[name='csrf-token']").attr ('content')
        }

    # +2013.3.20 tuiteraz
    # *2013.3.21 tuiteraz: + bShowMessOnSuccess
    sign_in: (sEmail,sPassword, bShowMessOnSuccess = true) ->
      hCSRF = j.CSRF()

      hData =
        utf8: "✓"
        juicy_user:
          email: sEmail
          password: sPassword
      hData[hCSRF.sParam] = hCSRF.sToken

      hResAttrs= {}

      sLocale = $(document).getUrlParam('locale')
      sLocale = 'ru' if !defined sLocale

      $.ajax {
        type: 'POST'
        url: "juicy_users/sign_in/?locale=#{sLocale}"
        dataType: 'json'
        async: false
        data: hData
        error: (data,textStatus,jqXHR) =>
         response = j.parse_JSON data.responseText
         oiSysMess.show response.Alerts, true if defined response.Alerts
         hResAttrs = response
        success: (data,textStatus,jqXHR) =>
         response = data
         oiSysMess.show(response.Alerts) if defined(response.Alerts) and bShowMessOnSuccess
         hResAttrs = response

        }
      return hResAttrs

    # конвертируем:
    # refbook -> Refbook
    # main-attr -> MainAttr
    # main-attr-other -> MainAttrOther
    # +2012.12.15 tuiteraz
    convert_section_to_obj_name: (sSection) ->
      #jconsole.enable_log "j.convert_section_to_obj_name(#{sSection})"
      sRes    = ''
      aTokens = sSection.match /([\w]+)/
      #jjconsole.log "aTokens-/([\w]+)/=%s",aTokens
      if _.isArray aTokens
        if aTokens[0] == sSection # в [0]  найденная строка - должна совпадать с оригинальной
          sRes = _.capitalize aTokens[1]
          #jjconsole.log "sRes=%s",sRes
          return sRes

      aTokens = sSection.match /([\w]+)-(\w+)/
      #jjconsole.log "aTokens-/([\w]+)-(\w+)/=%s",aTokens
      if _.isArray aTokens
        if aTokens[0] == sSection # в [0]  найденная строка - должна совпадать с оригинальной
          sRes = _.capitalize(aTokens[1]) + _.capitalize(aTokens[2])
          #jjconsole.log "sRes=%s",sRes
          return sRes

      aTokens = sSection.match /([\w]+)-(\w+)-(\w+)/
      #jjconsole.log "aTokens-/([\w]+)-(\w+)/=%s",aTokens
      if _.isArray aTokens
        if aTokens[0] == sSection # в [0]  найденная строка - должна совпадать с оригинальной
          sRes = _.capitalize(aTokens[1]) + _.capitalize(aTokens[2]) + _.capitalize(aTokens[3])
          #jjconsole.log "sRes=%s",sRes
          return sRes

    # объединить параметры атрибутов формы полученные с сервера с клиентскими
    # hCltElems = hCltElems + hSrvElems
    # - hCltElems : настройки элемента взятые из настройки формы
    # - hSrvElems : настройки элемента переданные сервером
    # - aOnlyParams : массив атрибутов настройки, которые принимают значения принудительно серверными значениями
    #                 если на сервере они были установлены
    # +2013.1.10 tuiteraz
    # *2013.1.29 tuiteraz: добавил параметр - массив добавляемых серверных атрибутов
    combine_elems_attrs: (hCltElems,hSrvElems, aOnlyParams='') ->
      #jconsole.enable_log "j.combine_elems_attrs"
      if _.isObject hSrvElems
        for sName,hAttrs of hCltElems
          if !_.isArray aOnlyParams
            _.extend hAttrs, hSrvElems[sName] if _.isObject hSrvElems[sName]
          else
            #jjconsole.log "aOnlyParams=%s",aOnlyParams
            #jjconsole.log "hAttrs(1)=%s",hAttrs
            #jjconsole.log "hSrvElems[#{sName}]=%s",hSrvElems[sName]
            for sParamName in aOnlyParams
              hAttrs[sParamName] = hSrvElems[sName][sParamName] if defined hSrvElems[sName][sParamName]
          #jjconsole.log "hAttrs(2)=%s",hAttrs

    # получить координыта и размеры jQuery объекта
    # +2013.1.22 tuiteraz
    get_coordinates_of: (jObj) ->
      hRes =
        iWidth:  $(jObj).width()
        iHeight: $(jObj).height()
        iLeft:   $(jObj).offset().left
        iTop:    $(jObj).offset().top

    # +2013.3.12 tuiteraz
    parse_bool: (str) ->
      return /^true$/i.test(str)


    # парсим строку JSON с помощью jQuery через исключения
    # +2013.1.11 tuiteraz
    parse_JSON: (sText) ->
      #jconsole.enable_log "j.parse_json"
      try
        jRes = $.parseJSON sText
      catch jErr
        jjconsole.log "jErr=%s",jErr
#        oiSysMess.show { sType:'error',sTitle:'JSON', sMessage:"Ошибка обработки ответа сервера" }, true
        jRes = {}
      return jRes

    frm :
      # +2013.2.23 tuiteraz
      is_checked: (sId) ->
        jChBox = $("##{sId}")
        if jChBox.length > 0
          bRes = jChBox[0].checked
        else
          bRes = false

        return bRes

      element:

        # +2013.2.23 tuiteraz
        clear: (hElement) ->
          if _.isObject(hElement)
            jObj = $("##{hElement.id}")
            if hElement.sInputStyle == FRM.input.style.input
              jObj.val ''
            else if hElement.sInputStyle == FRM.input.style.text
              jObj.val ''
            else if hElement.sInputStyle == FRM.input.style.checkbox
              jObj[0].checked = false
            else if hElement.sInputStyle == FRM.input.style.select
              jObj.attr 'value', ''
            else if (hElement.sInputStyle == FRM.input.style.radio_v) or (hElement.sInputStyle == FRM.input.style.radio_h)
              # сначала удалим все установки существующие
              $("input[name='#{hAttrs.id}']").removeAttr 'checked'

          else
            jconsole.enable_log "j.clear()"
            jconsole.warn "param is wrong: %s",hElement

        # получить значение из одного элемента
        # +2013.3.1 tuiteraz
        # *2013.3.4 tuiteraz: для списка если нет атрибута data-opt-code, то проверяем data-opt-link, value
        # *2013.3.21 tuiteraz: bDataRequired переносится из hElement
        # *2013.3.25 tuiteraz: + sId = null
        # *2013.6.2 tuiteraz: res << bRequired , sValueType, sElementId
        get_data: (hElement, sId = null) ->
          sSelector = if _.isNull sId then "##{hElement.id}" else "##{sId} .#{hElement.sIdClass}"
          jObj = $(sSelector)
          if hElement.sInputStyle == FRM.input.style.input
            #sValue = jObj.attr 'value'
            sValue = jObj.val()
          else if hElement.sInputStyle == FRM.input.style.text
            #sValue = jObj.attr 'value'
            sValue = jObj.val()
          else if hElement.sInputStyle == FRM.input.style.checkbox
            sValue = jObj[0].checked
          else if (hElement.sInputStyle == FRM.input.style.radio_v) or (hElement.sInputStyle == FRM.input.style.radio_h)
            jObj = $("input[name='#{hElement.id}']:checked")
            sValue = jObj.attr 'data-opt-code'
          else if hElement.sInputStyle == FRM.input.style.select
            if jObj[0].tagName == "SELECT"
              iIdx = jObj[0].selectedIndex
              oSelected_option = jObj[0].options[iIdx]
              if $(oSelected_option).hasAttr 'data-opt-code'
                sValue = $(oSelected_option).attr 'data-opt-code'
              else if $(oSelected_option).hasAttr 'data-opt-link'
                sValue = $(oSelected_option).attr 'data-opt-link'
              else
                sValue = $(oSelected_option).attr 'value'
            else
              sValue = null

          hRes =
            Value         : sValue
            sLabel        : hElement.sLabel
            sFullName     : hElement.sFullName
            sDataType     : hElement.sDataType
            sValueType    : hElement.sValueType
            bRequired     : hElement.bRequired
            sElementId    : hElement.id

          return hRes

        # проверяет содержит ли переданный эелемент указанное значение
        # +2013.3.1 tuiteraz
        has_value: (hElement,sTrackValue) ->
          hRes = j.frm.element.get_data hElement
          bRes = if hRes.Value == sTrackValue then true else false


        # +2013.2.4 tuiteraz
        hidden: (hElement) ->
          sId = if defined hElement.sCGid then hElement.sCGid else hElement.id
          $("##{sId}:hidden").length == 1

        # +2013.2.23 tuiteraz
        hide: (hElement, bClearAfterHide=false) ->
          if _.isObject(hElement) and defined(hElement.sCGid)
            $("##{hElement.sCGid}").slideUp 'fast', =>
              j.frm.element.clear hElement if bClearAfterHide
          else
            jconsole.enable_log "j.hide()"
            jconsole.warn "param is wrong: %s",hElement

        # +2013.2.23 tuiteraz
        show: (hElement) ->
          if _.isObject(hElement) and defined(hElement.sCGid)
            $("##{hElement.sCGid}").slideDown 'fast'
          else
            jconsole.enable_log "j.show()"
            jconsole.warn "param is wrong: %s",hElement

        # +2013.2.4 tuiteraz
        visible: (hElement) ->
          sId = if defined hElement.sCGid then hElement.sCGid else hElement.id
          $("##{sId}:visible").length == 1

    # +2013.3.17 tuiteraz
    current_user: (hUser=nil) ->
      if defined hUser
        window.CURRENT_USER = hUser
      else
        return CURRENT_USER

    # +2013.3.17 tuiteraz
    delete_current_user: ->
      delete window.CURRENT_USER

