define [
  'libs/dataview/jvm-templates'
  'libs/dataview/helpers/j'
  'jvm-extensions'
],(tmpl) ->
  me = this



  # will trigger label > i.icon-check|icon-check-empty on hidden input change state
  # sSelector - input
  # инпут должен находится в контейнере div, который содержит { input label i.icon*}
  # +2013.9.10 tuiteraz
  window.enable_fa_checkbox_for = (sSelector='', hOptions=null) ->
    $(document).delegate "#{sSelector}","change", (e)->
      # don't work in ie8
      jIcon = $(this).parent().children("label").children("i")
      if $(this)[0].checked
        jIcon.addClass "icon-check"
        jIcon.removeClass "icon-check-empty"
      else
        jIcon.removeClass "icon-check"
        jIcon.addClass "icon-check-empty"

    if is_ie 8
      $(sSelector).parent().children("label").click (e)->
        jInput = $(this).parent().children('input')
        jInput[0].checked = !jInput[0].checked

        jIcon = $(this).parent().children("label").children("i")
        if jInput[0].checked
          jIcon.addClass "icon-check"
          jIcon.removeClass "icon-check-empty"
        else
          jIcon.removeClass "icon-check"
          jIcon.addClass "icon-check-empty"

  # трансформирум <select> через chosen.js
  # sSelector - определяющий контекст селектор
  # hOptions - хэш опций:
  #   allow_single_deselect: true
  #   no_results_text: "No results matched"
  # +2013.3.7 tuiteraz
  # *2013.4.3 tuiteraz: Отключил временно потому что в контрольной панели глючит при выборе конфы
  window.enable_chosen_select_for = (sSelector='', hOptions=null) ->
    hOptions ||= {}
    hOptions.no_results_text = "Ничего не выбрано" if !defined hOptions.no_results_text
    hOptions.allow_single_deselect = true if !defined hOptions.allow_single_deselect
    #$("#{sSelector} select").chosen hOptions

  # запуск слимскрола к селектору с дополнительными настройками
  # - iHeight - фиксированная высота элемента, но если передается Hash,
  #   то предполагается, что в нем сустановлено свойство height и тогда
  #   hOptions =  iHeight
  # - hOptions - опции слимскорла:
  #   width - Width in pixels of the visible scroll area. Stretch-to-parent if not set. Default: none
  #   height - Height in pixels of the visible scroll area. Also supports auto to set the height to same as parent container. Default: 250px
  #   size - Width in pixels of the scrollbar. Default: 7px
  #   position - left or right. Sets the position of the scrollbar. Default: right
  #   color - Color in hex of the scrollbar. Default: #000000
  #   alwaysVisible - Disables scrollbar hide. Default: false
  #   distance - Distance in pixels from the edge of the parent element where scrollbar should appear. It is used together with position property. Default:1px
  #   start - top or bottom or $(selector) - defines initial position of the scrollbar. When set to bottom it automatically scrolls to the bottom of the scrollable container. When HTML element is passed, slimScroll defaults to offsetTop of this element. Default: top.
  #   wheelStep - Integer value for mouse wheel delta. Default: 20
  #   railVisible - Enables scrollbar rail. Default: false
  #   railColor - Sets scrollbar rail color, Default: #333333
  #   railOpacity - Sets scrollbar rail opacity. Default: 0.2
  #   allowPageScroll - Checks if mouse wheel should scroll page when bar reaches top or bottom of the container. When set to true is scrolls the page.Default: false
  #   scrollTo - Jumps to the specified scroll value. Can be called on any element with slimScroll already enabled. Example: $(element).slimScroll({ scrollTo: '50px' });
  #   scrollBy - Increases/decreases current scroll value by specified amount (positive or negative). Can be called on any element with slimScroll already enabled. Example: $(element).slimScroll({ scrollBy: '60px' });
  #   disableFadeOut - Disables scrollbar auto fade. When set to true scrollbar doesn't disappear after some time when mouse is over the slimscroll div.Default: false
  #   touchScrollStep  - Allows to set different sensitivity for touch scroll events. Negative number inverts scroll direction.Default: 200#
  # +2013.3.10 tuiteraz
  # +2013.4.7 tuiteraz: hOptions.sMarginCSS from caller
  window.enable_slim_scroll_for = (sSelector, iHeight, hOptions=null) ->
    if _.isObject iHeight
      hOptions = iHeight
      sHeight = if defined hOptions.height then hOptions.height else "108px"
    else
      sHeight = "#{iHeight}px"

    hOptions ||= {}
    hOptions.railVisible ||= true
    hOptions.height ||= sHeight
    hOptions.start ||= "top"
    sMarginCSS = hOptions.sMarginCSS if defined hOptions.sMarginCSS

    iHeight = parseFloat sHeight
    iBottom = parseFloat($(sSelector).css('padding-bottom')) + parseFloat($(sSelector).css('margin-bottom'))
    sWidth = $(sSelector).css('width')
    if $(sSelector)[0].tagName == "TEXTAREA"
      sNewHeight = "#{iHeight}px"
      sResizeCSS = 'none'
      sMarginCSS = ""
    else
      sNewHeight = "#{iHeight - iBottom - 2}px"
      sResizeCSS = ''
      sMarginCSS = ''


    $(sSelector).slimScroll hOptions
    $(sSelector).css {
      height: sNewHeight # компенсация margin & padding
      overflow: "hidden !important"
      width: sWidth + " !important"
      resize: sResizeCSS
      margin: sMarginCSS
    }

    $(sSelector).mousewheel ->
      hide_all_tooltips()
    $(".slimScrollBar").on {
      drag: ->
        hide_all_tooltips()
    }

  window.get_y_offset = ->
    if typeof(window.pageYOffset)=='number'
      iPageY=window.pageYOffset
    else
      iPageY=document.documentElement.scrollTop

    return iPageY


# после изменения содержимого селекта обновляем chosen.js для него
  # +2013.3.7 tuiteraz
  window.update_chosen_select_for = (Selector) ->
    $(Selector).trigger "liszt:updated" # не забываем обновить chosen.js

  # фокусируемся на первом инпуте в форме
  # +2013.1.18 tuiteraz
  window.autofocus_for = (sSelector) ->
    sSelectorInput = "#{sSelector} input:enabled:visible:not([readonly]):first"
    $(sSelectorInput).focus()
    if !$(sSelectorInput).is ':focus'
      sSelectorText = "#{sSelector} textarea:enabled:visible:not([readonly]):first"
      $(sSelectorText).focus()

  # +2013.1.12 tuiteraz
  window.GUID = ->
    f = (c) ->
      r = Math.random()*16|0
      v = if c == 'x' then r else r&0x3|0x8
      return v.toString(16)
    'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, f


  # +2012.11.11 Tuiteraz
  window.defined = (val) ->
    #console.enable_log "defined()"
    #console.log "val=%s",val
    #console.log "typeof val=%s",typeof val
    if typeof(val) == 'undefined'
      res =  false
    else if typeof(val) == 'string' and val.length==0
      res =  false
    else if typeof(val) == "object"
      res = !_.isNull val
    else
      res = true

  # для ссылок <a>|<button> заключенных в ul > li проверяем класс родительского li
  # если disabled, then return true, else return false
  # +2013.2.7 tuiteraz
  window.nav_item_disabled = (sAction) ->
    sSelector = "li>[data-action-id='#{sAction}']:first"
    $(sSelector).parent().hasClass 'disabled'

  # скроем все кнопки которые расположены в заголовках групп аккордиона
  #  вызывается при клике на группу, чтобы скрыть все остальные кнокпи
  # которые должны быть видимы только при активной группе
  # sSelector - селектор корнегого div аккордиона, кнопки которого обрабатываем
  # +2013.2.16 tuiteraz
  # *2013.2.18 tuiteraz: +sSelector
  window.hide_all_accordion_header_btns = (sSelector) ->
    $("#{sSelector} .accordion-heading a.btn").fadeOut()

  # подсветить спец классов активную группу аккордиона
  # sAccId - id аккордиона без '#'
  # +2013.2.28 tuiteraz
  window.colorize_curr_accordion_group = (sAccId) ->
    # добавим активной групппе класс колоризации
    # у всех остальных уберем его
    sSelector1 = "##{sAccId} .accordion-body:not(.in)"
    sSelector2 = "##{sAccId} .accordion-body.in"
    $(sSelector1).parent().removeClass 'accordion-in'
    $(sSelector2).parent().addClass 'accordion-in'


  # хелпер-обертка для описания реакции клика на кнопку
  # если в sTag передается функция, то принимаем что sTag='a'
  # +2012.11.11 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.1.21 Tuiteraz: comment e.stopPropagation()
  window.btn_on_click = (sAction, sTag, fn='') ->
    #console.enable_log "btn_on_click(#{sAction})"
    #console.log "typeof(sTag)=#{typeof(sTag)}"
    if $.isFunction(sTag)
      fn = sTag
      sTag = 'a'

    sSelector = "#{sTag}[data-action-id=#{sAction}]"
    #console.log "sSelector=#{sSelector}"

    $(sSelector).unbind('click').on {
    click: (e) ->
      #e.stopPropagation()
      #console.enable_log 'window.btn_on_click()'
      #console.log "click on #{sAction}"
      fn()
    }

  # обертка для отправки сообщения объекту
  # продолжение выполнения прерывается до заверешения отработки реакции
  # *2012.12.12 Tuiteraz
  window.send_event_to = (oiObj, hDetails, sType='click') ->
    $(oiObj).trigger sType, hDetails

  # хелпер-обертка для описания реакции клика на закладку
  # sSelector - селектор элемента .tabbable
  # +2012.11.13 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.2.16 Tuiteraz: передавать вызываемой функции контекст кликнутого объекта
  window.tab_on_click = (sSelector, fn) ->
    #console.enable_log "tab_on_click()"
    #console.log "sSelector=#{sSelector}"
    jTabs = $(sSelector)
    jA = jTabs.find '> ul.nav li a'
    #console.log "links count=#{jA.length}"

    $(jA).on {
    click: (e) ->
      #e.stopPropagation()
      #console.enable_log 'window.btn_on_click()'
      #console.log "click on #{s_action_id}"
      fn($(this))
    }

  # Установить класс active первой закладке для .tabbable
  # sSelector - путь до корневого .tabbable
  # +2012.11.13 Tuiteraz
  # *2012.12.12 Tuiteraz
  window.reset_tabs_state = (sSelector) ->
    #console.enable_log 'reset_tabs_class()'
    #console.log 'reseting tabs state...'
    sSelector1 = "#{sSelector} > ul.nav li"
    #console.log "sSelector=#{sSelector}"
    jLi = $(sSelector1)
    jLi.removeClass 'active'
    jLi.first().addClass 'active'

    sSelector2 = "#{sSelector} .tab-content > div"
    #console.log "sSelector=#{sSelector}"
    jDiv = $(sSelector2)
    jDiv.removeClass 'active'
    jDiv.first().addClass 'active'

  # удалить привязку тултипов звёздочек обязательных полей
  # +2012.11.13 Tuiteraz
  # *2012.12.12 Tuiteraz
  window.destroy_tooltip_for_required = ->
    jReq = $('.required-field-marker')
    jReq.tooltip 'destroy' if defined jReq

  # включить поддержку подсказки длины введенного текста для всех показанных инуптов и текста
  # +2013.1.22 tuiteraz
  # *2013.3.7 tuiteraz: exclude chosen.js input search
  window.enable_length_tooltip = ->
    render_tooltip= ->
      sHtml = """
        <div id="#{LENGTH_TOOLTIP.id}">
          <div class="content" ></div>
          <div class="arrow" ></div>
        </div>
      """
      $("body").append sHtml


    get_tooltip = ->
      jRes = $("##{LENGTH_TOOLTIP.id}")
      if jRes.length==0
        render_tooltip()
        jRes = $("##{LENGTH_TOOLTIP.id}")
      return jRes

    hide_length_tooltip = ->
      jLT = get_tooltip()
      jLT.fadeOut 'fast'

    show_length_tooltip = (jInput, bNoRefresh=false) ->
      clearTimeout window.juicy_length_tooltip_timeout

      #console.enable_log "enable_length_tooltip(show_length_tooltip)"
      jLT = get_tooltip()
      #console.log "jLT=%s",jLT
      hCoord = j.get_coordinates_of jInput

      jLT.css {
        left: hCoord.iWidth + hCoord.iLeft - 10
        top: hCoord.iTop - 30
      }

      iTextSize = $(jInput).attr('value').length
      jLT.children(".content").text iTextSize
      #console.log "$(jInput).attr('value')=%s",$(jInput).attr('value')

      jLT.fadeIn 'slow'
      window.juicy_length_tooltip_timeout = setTimeout =>
        jLT.fadeOut 'slow'
      ,LENGTH_TOOLTIP.inactive_timeout

      if !bNoRefresh
        window.juicy_length_tooltip_refresh_timeout = setTimeout =>
          show_length_tooltip(jInput,true)
        , 100

    sCheckBox = ":not([type='#{FRM.input.type.checkbox}'])"
    sNum      = ":not([type='#{FRM.input.type.number}'])"
    sReadOnly = ":not([readonly='readonly'])"
    sChzInp = ":not([autocomplete])"
    sInpSel = "input#{sCheckBox}#{sNum}#{sReadOnly}#{sChzInp}"

    $("ul.nav li").unbind('click').on {
      click: ->
        hide_length_tooltip()
    }

    $(sInpSel).on {
      keypress: (e) ->
        show_length_tooltip(this) if !(e.keyCode==9)  #!tab

      focus: ->
        setTimeout =>
          show_length_tooltip(this)
        , LENGTH_TOOLTIP.after_focus_timeout
      blur: ->
        hide_length_tooltip()
    }

    $("textarea").on {
    keypress: (e) ->
      show_length_tooltip(this) if !(e.keyCode==9)  # !tab

    focus: ->
      setTimeout =>
        show_length_tooltip(this)
      , LENGTH_TOOLTIP.after_focus_timeout
    blur: ->
      hide_length_tooltip()

    }
    $(document).scroll ->
      hide_length_tooltip()



  # +2012.11.11 Tuiteraz
  # *2013.6.1 Tuiteraz: + sTooltip , +iShowTime
  window.enable_tooltip_for_required = (sTooltip='',iShowTime=2500) ->
    sTooltip = "Обязательно для заполнения" if !defined sTooltip
    $("a.required-field-marker[data-original-title]").attr "data-original-title", sTooltip
    $('.required-field-marker').tooltip {
    placement: "top"
    delay: {show: iShowTime, hide: 100}
    }
    $(".required-field-marker").unbind('click').click (e)->
      e.preventDefault()


  # удалить привязку тултипов кнопок
  # +2012.11.13 tuiteraz
  window.destroy_tooltip_for_btn = ->
    #j_btn = $('.btn')
    #j_btn.tooltip 'destroy' if defined j_btn
    $("div.tooltip").remove()

  # +2012.11.11 Tuiteraz
  # *2012.11.13 Tuiteraz
  # *2012.12.12 Tuiteraz
  window.enable_tooltip_for_btn = (sSelector='') ->
    #console.enable_log "enable_tooltip_for_btn()"
    #console.log "sSelector=#{sSelector}"
    $("#{sSelector}").tooltip {
    placement: "top"
    delay: {show: 2500, hide: 100}
    }

  # +2012.11.15 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.05.30 Tuiteraz: +iShowTime, iHideTime
  window.enable_tooltip_for = (sSelector='',iShowTime=2500, iHideTime=100) ->
    $("#{sSelector}").tooltip {
    placement: "bottom"
    delay: {show: iShowTime, iHideTime: 100}
    }

  # включаем для инпутов всплывающуюу подсказу снизу при наведении
  # если она была прорисована
  # Вызывать нужно после обновления данных формы для привязки реакций
  # sSelector - путь к диву формы/закладки для которой нужна активация функции
  #             уже включает символ #
  # +2013.1.18 tuiteraz
  window.enable_focusin_help_for = (sSelector) ->
    hlpr =
      on_focus: (jObj) ->
        jParent = $(jObj).parent()
        jHelpBlock = $(jParent).children(".help-block:first")
        jHelpBlock.slideDown()
      on_blur: (jObj) ->
        jParent = $(jObj).parent()
        jHelpBlock = $(jParent).children(".help-block:first")
        jHelpBlock.slideUp()


    sSelector += " .controls"
    $("#{sSelector} input").on {
      blur: ->
        hlpr.on_blur this
      focus: ->
        hlpr.on_focus this
    }

    $("#{sSelector} textarea").on {
    blur: ->
      hlpr.on_blur this
    focus: ->
      hlpr.on_focus this
    }


  # скрыть тултипы размера и обычные
  # !2012.12.12 Tuiteraz
  # *2013.3.11 Tuiteraz
  # *2013.4.3 Tuiteraz: * tooltip hide вместо tooltip.hide 'tooltip'
  window.hide_all_tooltips = ->
    #$(".tooltip").tooltip 'hide'
    $(".tooltip").hide()
    jLT = $("##{LENGTH_TOOLTIP.id}")
    jLT.fadeOut 'fast' if jLT.length >= 0

  # поддержка кнопок спиннера для ввода чисел
  # +2012.11.11 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.1.25 Tuiteraz: не должен работать на неактивном инпуте
  # *2013.1.30 Tuiteraz: нагружает проц интервальная проверка
  # *2013.2.11 Tuiteraz: $(jInput).trigger 'change' для отслеживания в модулях контекста
  window.enable_twbp_spinner = ->
    #jconsole.enable_log "enable_twbp_spinner()"
    #console.log "defined window.iSpinnerCheckIntervalId=%s",defined window.iSpinnerCheckIntervalId
    # вешаем проверку доступности инпута к которому привязан спиннер
    # чтобы активировать или деактивироть его кнопки
    check_spinners = ->
      #console.enable_log "enable_twbp_spinner(interval)"
      oiTime = new Date()
      sTime = "#{oiTime.getMinutes()}:#{oiTime.getSeconds()}"
      #console.log "(#{sTime})checking.."
      aSpinnerBtns = $(".spinner-btn-up:visible")
      #console.log "[#{aSpinnerBtns.length}] spinner up btns found"
      for jSpinnerBtn in aSpinnerBtns
        sInpId = $(jSpinnerBtn).attr "data-target-input-id"
        #console.log "input id = #{sInpId}"
        jInput = $("##{sInpId}")
        if jInput.hasClass('disabled') or jInput.hasAttr('readonly')
          $(jSpinnerBtn).addClass 'disabled'
        else
          $(jSpinnerBtn).removeClass 'disabled'

      aSpinnerBtns = $(".spinner-btn-down:visible")
      for jSpinnerBtn in aSpinnerBtns
        sInpId = $(jSpinnerBtn).attr "data-target-input-id"
        jInput = $("##{sInpId}")
        if jInput.hasClass('disabled') or jInput.hasAttr('readonly')
          $(jSpinnerBtn).addClass 'disabled'
        else
          $(jSpinnerBtn).removeClass 'disabled'

    if !defined window.iSpinnerCheckIntervalId
      #console.log "setting interval execution..."
      window.iSpinnerCheckIntervalId = setInterval =>
        check_spinners()
      , DATAVIEW.spinner_btn_refresh_state_time

    # вешаем обработчик кликов
    $(".spinner-btn-up").unbind('click').on {
    click:(e) ->
      #jconsole.enable_log "enable_twbp_spinner(up click)"
      jInput = $(this).parent().parent().children("input:first")
      if !jInput.hasClass('disabled') && !jInput.hasAttr('readonly')
        e.preventDefault()
        e.stopPropagation()
        sInputId = $(this).attr 'data-target-input-id'
        jInput = $("##{sInputId}")
        iCurrValue = parseInt $(jInput).val()
        iCurrValue = if isNaN(iCurrValue) then 0 else iCurrValue
        iCurrValue += 1
        $(jInput).val(iCurrValue.toString())
        $(jInput).trigger 'change'
      else
        e.preventDefault()
    }

    $(".spinner-btn-down").unbind('click').on {
    click:(e) ->
      jInput = $(this).parent().parent().children("input:first")
      if !jInput.hasClass('disabled') && !jInput.hasAttr('readonly')
        e.preventDefault()
        e.stopPropagation()

        sInputId = $(this).attr 'data-target-input-id'
        jInput = $("##{sInputId}")
        iCurrValue = parseInt $(jInput).val()
        iCurrValue = if isNaN(iCurrValue) then 0 else iCurrValue
        iCurrValue -= 1
        $(jInput).val(iCurrValue.toString())
        $(jInput).trigger 'change'
      else
        e.preventDefault()

    }

#----( DATAVIEW ---------------------------------------------------------------------
  dialog:
    alert: (sText='',sTitle='Сообщение') ->
      hOk      = DATAVIEW.dialog.buttons.ok

      dlg_hide = ->
        $("##{DATAVIEW.dialog.id}").modal 'hide'

      dlg_html_exist= ->
        $("##{DATAVIEW.dialog.id}").length > 0 ? true : false

      dlg_render= ->
        $("##{DATAVIEW.dialog.id}").remove() if dlg_html_exist()

        sHtml = tmpl.div "modal hide fade", DATAVIEW.dialog.id, [
          tmpl.div "modal-header", [
            tmpl.html_tag "button", "close", "", "data-dismiss='modal' aria-hidden='true'", "&times;"
            tmpl.h 3, sTitle
          ]
          tmpl.div "modal-body", [
            sText
          ]
          tmpl.div "modal-footer", [
            tmpl.icon_btn hOk.sIcon, hOk.sAction, '', hOk.sTitle, hOk.sClass
          ]

        ]
        $("body").append sHtml
        $("##{DATAVIEW.dialog.id}").modal "show"

      dlg_remove= ->
        $("##{DATAVIEW.dialog.id}").remove()

      dlg_bind_events = ->
        btn_on_click hOk.sAction, ->
          dlg_hide()
          dlg_remove()

      dlg_render()
      dlg_bind_events()

    # показать диалог с вопросом и кнопками дождаться ответа и вернуть действие из кнопки
    # диалог всегда осздается перед показом, а после ответа удаляется
    # aBtnStyles - [sYesBtnStyle,sNoBtnStyle,sCancelBtnStyle]
    #   sYesBtnStyle - 'success'|'warning'|'danger'|'primary'|'info'|'inverse'
    #   определяет приставку к дополнительному классу типа btn-primary
    # +2013.2.1 tuiteraz
    # *2013.3.12 tuiteraz: +bNoCancelBtn
    ask: (sTitle='',sText='', aBtnStyles=[],fnYes,fnNo,fnCancel,bNoCancelBtn=false) ->
      #jconsole.enable_log "jvmHelpers (dialog.ask)"

      hYes     = DATAVIEW.dialog.buttons.yes
      hNo      = DATAVIEW.dialog.buttons.no
      hCancel  = DATAVIEW.dialog.buttons.cancel

      dlg_hide = ->
        $("##{DATAVIEW.dialog.id}").modal 'hide'

      dlg_html_exist= ->
        $("##{DATAVIEW.dialog.id}").length > 0 ? true : false

      dlg_render= ->
        $("##{DATAVIEW.dialog.id}").remove() if dlg_html_exist()

        sYesStyle    = if aBtnStyles.length>=1 then " btn-#{aBtnStyles[0]}" else ''
        sNoStyle     = if aBtnStyles.length>=2 then " btn-#{aBtnStyles[1]}" else ''
        sCancelStyle = if aBtnStyles.length==3 then " btn-#{aBtnStyles[2]}" else ''

        if bNoCancelBtn
          sCancelBtnHtml = ''
        else
          sCancelBtnHtml = tmpl.icon_btn hCancel.sIcon, hCancel.sAction, '', hCancel.sTitle, hCancel.sClass+sCancelStyle

        sHtml = tmpl.div "modal hide fade", DATAVIEW.dialog.id, [
                   tmpl.div "modal-header", [
                     tmpl.html_tag "button", "close", "", "data-dismiss='modal' aria-hidden='true'", "&times;"
                     tmpl.h 3, sTitle
                   ]
                   tmpl.div "modal-body", [
                     sText
                   ]
                   tmpl.div "modal-footer", [
                     tmpl.icon_btn hYes.sIcon, hYes.sAction, '', hYes.sTitle, hYes.sClass+sYesStyle
                     tmpl.icon_btn hNo.sIcon, hNo.sAction, '', hNo.sTitle, hNo.sClass+sNoStyle
                     sCancelBtnHtml
                   ]

        ]
        #console.log "sHtml1=#{sHtml}"
        $("body").append sHtml
        $("##{DATAVIEW.dialog.id}").modal "show"

      dlg_remove= ->
        $("##{DATAVIEW.dialog.id}").remove()

      dlg_bind_events = ->
        btn_on_click hYes.sAction, ->
          #console.enable_log "jvmHelpers (dialog.ask: YES click)"
          #console.log 'Yes'

          dlg_hide()
          fnYes() if _.isFunction fnYes
          dlg_remove()

        btn_on_click hNo.sAction, ->
          #console.enable_log "jvmHelpers (dialog.ask: NO click)"
          #console.log 'No'
          dlg_hide()
          fnNo() if _.isFunction fnNo
          dlg_remove()


        btn_on_click hCancel.sAction, ->
          #console.enable_log "jvmHelpers (dialog.ask: CANCEL click)"
          #console.log 'Cancel'
          dlg_hide()
          fnCancel() if _.isFunction fnCancel
          dlg_remove()

      dlg_render()
      dlg_bind_events()

    # показать диалог авторизации и залогинить пользователя по кнопке
    # диалог всегда осздается перед показом, а после ответа удаляется
    # +2013.3.21 tuiteraz
    sign_in: (sText='', sEmail='') =>
      me = this
      hSignIn     = DATAVIEW.dialog.buttons.sign_in
      if !_.isEmpty sText
        sLabelHtml = tmpl.div "alert alert-info", "", [
          sText
        ]
      else
        sLabelHtml = ''

      dlg_hide = ->
        $("##{DATAVIEW.dialog.id}").modal 'hide'

      dlg_html_exist= ->
        $("##{DATAVIEW.dialog.id}").length > 0 ? true : false

      dlg_render= ->
        $("##{DATAVIEW.dialog.id}").remove() if dlg_html_exist()

        sHtml = tmpl.div "modal hide fade modal-w370 dialog-sign-in", DATAVIEW.dialog.id, [
          tmpl.div "modal-header", [
            tmpl.h 3, "Авторизация"
          ]
          tmpl.div "modal-body", [
            tmpl.div "w350","", [
              tmpl.form "form-horizontal","", [
                sLabelHtml
                tmpl.twbp_input DATAVIEW.dialog.elements.sign_in.email
                tmpl.twbp_input DATAVIEW.dialog.elements.sign_in.password
              ]
            ]
          ]
          tmpl.div "modal-footer", [
            tmpl.icon_btn hSignIn.sIcon, hSignIn.sAction, '', hSignIn.sTitle, hSignIn.sClass
          ]

        ]

        $("body").append sHtml
        $("##{DATAVIEW.dialog.elements.sign_in.email.id}").val sEmail if !_.isEmpty sEmail
        $("##{DATAVIEW.dialog.id}").modal {
          show: true
          keyboard: false
          backdrop: "static"
        }

      dlg_remove= ->
        $("##{DATAVIEW.dialog.id}").remove()

      dlg_bind_events = =>
        btn_on_click hSignIn.sAction, =>
          hElemsData = j.get_elements_data DATAVIEW.dialog.elements.sign_in
          bRes = j.elements_data_is_valid hElemsData
          if bRes == true
            hResponse = j.sign_in hElemsData.email.Value, hElemsData.password.Value
            bRes = false if !hResponse.bSuccess
          else
            oiSysMess.show bRes, true
            bRes = false

          if bRes == true
            j.current_user hResponse.hCurrentUser if defined hResponse.hCurrentUser
            dlg_hide()
            dlg_remove()


      dlg_render()
      dlg_bind_events()

#----) DATAVIEW ---------------------------------------------------------------------

  # После того как добавили новый элемент в списке любом его надо красиво показать
  # а потом очистить его атрибуты, чтобы второй раз на него не нарваться
  # +2012.11.12 Tuiteraz
  # *2013.3.11 Tuiteraz: после показа добавить атрибут data-to-bind=now для поиска при привязке слушателей
  show_just_created_li: ->
    $("li[data-to-show='now']").slideDown 'fast', ->
      $(this).removeAttr("style")
      $(this).removeAttr("data-to-show")

  # !2012.12.12 Tuiteraz: refactoring
  # *2013.3.17 Tuiteraz
  current_locale: ->
    sLocale = $(document).getUrlParam('locale')
    sLocale = 'ru' if !defined sLocale
    return sLocale

#----( CONFMAN ---------------------------------------------------------------------

  # вызывается при клике на списке в main-navbar, чтобы скрыть закладки у которых принудительно
  # был установлен style, когда я их красиво показывал и обновлял
  # например когда мы щелкаем по справочникам, то используется одна и та же закладка,
  # но когда мы переходим к константам, то там уже другие и закладка справочника остается,
  # потому чтоу неё установлен принудительный стиль
  # +2012.11.10 Tuiteraz
  # *2012.11.11 Tuiteraz
  clear_tabs_style: ->
    #console.enable_log "jvm-helpers (clear_tabs_style)"
    jTabs = $("div.tab-pane")
    #console.log "tabs count=#{jTabs.length}"
    jTabs.removeAttr 'style'


  # Удалить объект конфы. Сервер заботится о том, чтобы удалить и его данные
  # Указываем запрос, по которому искать объект, конфу и контроллер, к которому отправляется запрос
  # sConfmanSection - refbook, constant - по сути названия контроллеров confman
  # +2012.11.12 Tuiteraz
  # *2012.12.12 Tuiteraz: refactoring
  # *2013.4.3 Tuiteraz: добавил способность обращаться  клюбому контроллеру
  destroy_object: (hQuery, sConfId, sSection, sController='confman') ->
    bSuccess = false # в эту переменную вернутся атрибуты с сервера, которыми нужно обновить форму

    me = this

    $.ajax {
    type: 'POST'
    url: "#{sController}/#{sSection}/?locale=#{@current_locale()}"
    dataType: 'json'
    data: {sConfId: sConfId, sOperation: 'destroy', hQuery: hQuery}
    async: false
    context : document.body
    error: (data,textStatus,jqXHR) =>
      response = data
      oiSysMess.show response.Alerts, true if defined response.Alerts
    success: (data,textStatus,jqXHR) =>
      response = data
      oiSysMess.show(response.Alerts,true) if defined response.Alerts
      bSuccess = response.bSuccess

    }
    return bSuccess

  # Удалить main attr объекта конфы. Сервер заботится о том, чтобы удалить и его данные
  # Указываем запрос, по которому искать атриубт, объект, конфу и контроллер, к которому отправляется запрос
  # sConfmanSection - refbook, constant - по сути названия контроллеров confman
  # sObjectId - id объекта родительского. Контроллер знает чей это айди
  # +2012.11.16 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.1.16 Tuiteraz
  destroy_object_main_attr: (hQuery, sObjectId, sConfId, sConfmanSection) ->
    bSuccess = false # в эту переменную вернутся атрибуты с сервера, которыми нужно обновить форму

    me = this

    hData =
      sConfId: sConfId
      sOperation: 'destroy-main-attr'
      hQuery: hQuery
    hData["s#{j.convert_section_to_obj_name sConfmanSection}Id"] = sObjectId

    $.ajax {
    type: 'POST'
    url: "confman/#{sConfmanSection}/?locale=#{@current_locale()}"
    dataType: 'json'
    data: hData
    async: false
    context : document.body
    error: (data,textStatus,jqXHR) =>
      response = j.parse_JSON data.responseText
      oiSysMess.show response.Alerts, true if defined response.Alerts
    success: (data,textStatus,jqXHR) =>
      response = data
      oiSysMess.show(response.Alerts,true) if defined response.Alerts
      bSuccess = response.bSuccess
    }
    return bSuccess

  # !2012.12.12 Tuiteraz
  hide_all_dropdowns: ->
    $("li.dropdown").removeClass 'open'

  # вывести статус валидации для инпутов в переданном массиве = error
  # +2013.3.21 tuiteraz
  set_validation_state_for:(aAttrCodes,hElements) ->
    for sCode, hAttrs of hElements
      sAttrId = hElements[sCode].id
      jCG = $("##{sAttrId}").parent().parent()
      if _.contains aAttrCodes, sCode
        jCG.addClass "error"
      else
        jCG.removeClass "error"

  # очистить статус валидации для инпутов
  # +2013.3.21 tuiteraz
  reset_validation_state_for:(hElements) ->
    $("##{hElements[sCode].id}").parent().parent().removeClass "error" for sCode, hAttrs of hElements



  # отправить собранные из html данные на сервер для сохранения в монго
  # элементы - визуальное хранилище данных атрибутов реактируемого объекта
  # hElemsData[name]=
  #    value
  #    label
  #    data_type
  #    data_required
  # sConfId - id  конфы к которой обращаемся или [sConfId, refbook_id]
  # sConfmanSection - секция к которой будет обращаться функция: constant, refbook, ..
  # или [confman_section, 'main-attr']
  # +2012.11.10 Tuiteraz
  # *2012.11.16 Tuiteraz : поддержка массив в секции и айди
  # *2012.12.12 Tuiteraz
  # *2012.12.14 Tuiteraz
  # *2013.3.11 Tuiteraz
  # *2013.3.20 Tuiteraz: +sController='confman' for using in cp
  #                      * sConfmanSection -> sSection
  # *2013.3.21 tuiteraz: +bReturnFullResponse - возвращаться весь ответ сервера в JSON
  update_elements_data: (hElementsData, sConfId, sSection, sController='confman', bReturnFullResponse=false) ->
    hAttrData = j.parse_values_from_attr_hash hElementsData

    sOperation = 'create'
    sOperation = 'update' if hAttrData.id.length > 1

    sSectionId = sSubSection = ''

    if _.isObject(sConfId) and _.isObject(sSection)
      # sConfId приходит массив айди: конфы и справочника, например, их нужно раскидать по переменным
      # sConfmanSection приходит массив секций: refbook и main-attr, например, их нужно раскидать по переменным
      sSectionId      = sConfId[1]
      sConfId         = sConfId[0]
      sSubSection     = sSection[1]
      sSection        = sSection[0]

    hResAttrs = [] # в эту переменную вернутся атрибуты с сервера, которыми нужно обновить форму

    me = this

    hData =
      sConfId: sConfId
      hData: hAttrData

    if defined(sSectionId) and defined(sSubSection)
      sCapt = _(sSection).capitalize()
      hData["s#{sCapt}Id"] = sSectionId
      sOperation += "-#{sSubSection}"
      #sResponseObjectKey = sSubSection.replace /-/,'_'
      sResponseObjectKey = j.convert_section_to_obj_name(sSubSection)
    else
      sResponseObjectKey = j.convert_section_to_obj_name(sSection)

    hData["sOperation"] = sOperation

    $.ajax {
    type: 'POST'
    url: "#{sController}/#{sSection}/?locale=#{@current_locale()}"
    dataType: 'json'
    data: hData
    async: false
    context : document.body
    error: (data,textStatus,jqXHR) =>
      response = j.parse_JSON data.responseText
      oiSysMess.show response.Alerts, true if defined response.Alerts
      hResAttrs = response
    success: (data,textStatus,jqXHR) =>
      response = data
      oiSysMess.show(response.Alerts) if defined response.Alerts
      if defined(response[sResponseObjectKey]) && !bReturnFullResponse
        hResAttrs = response[sResponseObjectKey]
      else
        hResAttrs = response

    }
    return hResAttrs

#----) CONFMAN ---------------------------------------------------------------------

#----( CONF ------------------------------------------------------------------------
  # JuicyConf -> JuicyObject
  # отправить собранные из html данные на сервер для сохранения в монго
  # элементы - визуальное хранилище данных атрибутов реактируемого объекта
  # hElemsData[name]=
  #    Value
  # oiTab - tab object itseld
  # +2012.11.22 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2012.12.22 Tuiteraz
  update_object_data: (hElementsData, oiTab) ->
    #jconsole.enable_log "update_object_data"
    #console.log "hElementsData= %s", hElementsData
    hAttrData = {} # а вот это уже данные атрибутов сохраняемого объекта
    for sName, hAttrs of hElementsData
      hAttrData[sName]=hElementsData[sName].Value

    #console.log "hAttrData= %s", hAttrData

    sOperation = 'update' # всегда update, потому что объект уже усещусвует на момент редактирования

    me = this

    hData =
      sTabId          : oiTab.id
      hObjectQuery    : oiTab.hDataQuery
      sObjectClass    : oiTab.sDataClass
      hObjectAttrData : hAttrData

    #console.log "hData=%s",hData

    response = '' # в эту переменную вернутся атрибуты с сервера, которыми нужно обновить форму
    $.ajax {
    type: 'POST'
    url: "#{oiTab.sActionController}/update_tab/?locale=#{@current_locale()}"
    dataType: 'json'
    data: hData
    async: false
    context : document.body
    error: (data,textStatus,jqXHR) =>
      response = data
      oiSysMess.show response.Alerts, true if defined response.Alerts
    success: (data,textStatus,jqXHR) =>
      #console.enable_log "jvmConfman(get_constant:success)"
      response = data
      oiSysMess.show(response.Alerts) if defined response.Alerts
      #console.log 'response:%s',response
      #hResAttrs = response[sResponseObjectKey] if defined response[sResponseObjectKey]

    }
    return response

#----) CONF ------------------------------------------------------------------------





