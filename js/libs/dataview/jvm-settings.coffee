# !2012.12.26 tuiteraz
# *2013.3.17 tuiteraz
window.FRM=
  input:
    style:
      input: 'input'
      text: 'textarea'
      radio_v: 'radio-vertical'
      radio_h: 'radio-horizontal'
      select: 'select'
      checkbox: 'checkbox'
    type:
      hidden: 'hidden'
      text: 'text'
      number: 'number'
      checkbox: 'checkbox'
      password: 'password'
  list:
    action:
      view:
        id:      'view-item'
        icon:    'eye-open'
        tooltip: 'Открыть элемент'
      destroy:
        id:      'destroy-item'
        icon:    'trash'
        tooltip: 'Удалить элемент'

window.DATAVIEW=
  tabs_id : 'dataview-tabs'
  tabs_nav_id : 'dataview-tabs-nav'
  tabs_content_id : 'dataview-tabs-content'
  spinner_btn_refresh_state_time: 1000 # время обновления состояния кнопок спиннера
  dialog:
    id: "dataview-dialog"
    buttons:
      ok:
        id: "dataview-dialog-btn-ok"
        sIcon: "ok icon-white"
        sTitle: "Хорошо"
        sClass: "btn-primary"
        sAction: "dataview-dialog-ok"
      sign_in:
        id: "dataview-dialog-btn-sign-in"
        sIcon: "signin icon-white"
        sTitle: "Войти"
        sClass: "btn-primary"
        sAction: "dataview-dialog-sign-in"
      yes:
        id: "dataview-dialog-btn-yes"
        sIcon: "ok-sign icon-white"
        sTitle: "Да"
        sClass: ""
        sAction: "dataview-dialog-yes"
      no:
        id: "dataview-dialog-btn-no"
        sIcon: "remove-sign icon-white"
        sTitle: "Нет"
        sClass: ""
        sAction: "dataview-dialog-no"
      cancel:
        id: "dataview-dialog-btn-cancel"
        sIcon: "ban-circle"
        sTitle: "Отмена"
        sClass: ""
        sAction: "dataview-dialog-cancel"
    elements:
      sign_in:
        email:
          id: "dataview-dialog-sign-in-email"
          sLabel: 'E-mail:'
          sFullName: 'E-mail'
          sInputStyle: FRM.input.style.input
          sInputType: FRM.input.type.text
          sPlaceholder: '...'
          bRequired: true
          bDataRequired: true
        password:
          id: "dataview-dialog-sign-in-password"
          sLabel: 'Пароль:'
          sFullName: 'Пароль'
          sInputStyle: FRM.input.style.input
          sInputType: FRM.input.type.password
          sPlaceholder: '...'
          bRequired: true
          bDataRequired: true

  tmpl:
    main_div_id:    "dataview-main-container"
    #main_div_params: "style='margin-top:40px;'"
    main_div_params: ""
    header_navbar_user_panel:
      id: "header-navbar-user-panel-id"
      enterprise_path: '/enterprise'
      confman_path:    '/confman'
      cp_path:         '/cp'
      portal_path:     '/portal'
      subsman_path:    '/subsman'
    navbars:
      id: "dataview-navbars"
      toolbar:
        id: "dataview-navbars-toolbar"
    tabs:
      id: "dataview-tabs"
      nav_id: "dataview-tabs-nav"
      content_id: "dataview-tabs-content"

window.LENGTH_TOOLTIP=
  id: "juicy-input-length-tooltip"
  inactive_timeout: 5000 # время неактивности инпута после которого скрывается тултип
  after_focus_timeout: 1000 #  время задержки после фокуса на инпут, чтобы показать тултип




