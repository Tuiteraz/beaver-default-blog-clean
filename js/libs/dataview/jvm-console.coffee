define ->
  f_enabled = false
  log_context = 'GLOBAL'

  mod = this

  enable_log: (context='GLOBAL') ->
    window.bJvmConsoleEnabled = true
    window.sJvmConsoleLogContext = context

  disable_log: ->
    window.bJvmConsoleEnabled = false
    window.sJvmConsoleLogContext = 'GLOBAL'

  log: (msg,var1=null,var2=null,var3=null) ->
    if var1 != null
      var1 = JSON.stringify(var1)
    if var2 != null
      var2 = JSON.stringify(var2)
    if var3 != null
      var3 = JSON.stringify(var3)

    console.log("#{sJvmConsoleLogContext} :: #{msg}",var1,var2,var3) if bJvmConsoleEnabled

  info: (msg='',var1=null,var2=null,var3=null) ->
    if var1 != null
      var1 = JSON.stringify(var1)
    if var2 != null
      var2 = JSON.stringify(var2)
    if var3 != null
      var3 = JSON.stringify(var3)

    console.info("#{sJvmConsoleLogContext} :: #{msg}",var1,var2,var3) if bJvmConsoleEnabled

  debug: (msg,var1=null,var2=null,var3=null) ->
    if var1 != null
      var1 = JSON.stringify(var1)
    if var2 != null
      var2 = JSON.stringify(var2)
    if var3 != null
      var3 = JSON.stringify(var3)

    console.debug("[DEBUG] #{sJvmConsoleLogContext} :: #{msg}",var1,var2,var3) if bJvmConsoleEnabled

  #+2013.9.23 tuiteraz
  error: (msg,var1=null,var2=null,var3=null) ->
    if var1 != null
      var1 = JSON.stringify(var1)
    if var2 != null
      var2 = JSON.stringify(var2)
    if var3 != null
      var3 = JSON.stringify(var3)

    console.error("#{sJvmConsoleLogContext} :: #{msg}",var1,var2,var3) if bJvmConsoleEnabled


  warn: (msg,var1=null,var2=null,var3=null) ->
    if var1 != null
      var1 = JSON.stringify(var1)
    if var2 != null
      var2 = JSON.stringify(var2)
    if var3 != null
      var3 = JSON.stringify(var3)

    console.warn("#{sJvmConsoleLogContext} :: #{msg}",var1,var2,var3) if bJvmConsoleEnabled

  # + 2013.2.18 tuiteraz
  dir: (msg) ->
    console.dir(msg) if bJvmConsoleEnabled


  # + 2013.2.18 tuiteraz
  group: (sTitle) ->
    s1 = "#{sJvmConsoleLogContext} - #{JSON.stringify(sTitle)}"
    console.groupCollapsed(s1) if bJvmConsoleEnabled


  # + 2013.2.18 tuiteraz
  group_end: ->
    console.groupEnd() if bJvmConsoleEnabled





