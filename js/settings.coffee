define ['jvm-console'],(jconsole) ->

  jconsole.info "settings"

  window.STUBS = {}

  window.SITE =
    url_param_names:
      page: 'page'
      post: 'post'
      nav: 'nav'
    sBrowserMainTitle: "Default-Blog-Clean"
    iInnerContentWidth: 940 # from twbp fromcss
    site_title:
      container_id: "site-title-container"
      sTitle: "Мой блог"
      sSubTitle: "тема:'clean'"
    actions:
      goto_nav_link: "goto-nav-link"
      goto_post_link: "goto-post-link"
    top_slide_container_id: "top-slide-container"
    nav_container_id: "navigation-container"
    content_container_id: "content-container"
    sidebar:
      container_id: "sidebar-container"
    footer:
      container_id: "footer-container"
      sCredits: "Powered by <a href='http://qbera.ru'>«qbera»</a> 2013 ©"

    slider:
      sImagesBasePath: "images/slider"
      aImages: ["01.png", "02.png", "03.png", "04.png", "05.png"]
      iImageHeight: 300
    post:
      get_modes:
        length: 'length'
        data: 'data'
      modes:
        iExerptLength: 200
        exerpt: "exerpt"
        full: 'full'
