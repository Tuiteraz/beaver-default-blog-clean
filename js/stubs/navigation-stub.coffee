define ['jvm-console'],(jconsole) ->
  jconsole.info "navigation-stub"

  window.STUBS["NAVIGATION"] = [
    {
      sSlug: "index"
      sIcon: "home"
      sTitle: 'Главная'
      sBrowserTitle: 'Главная'
      bIsSinglePage: false
      bIsIndex: true
      aPostTags:[] # фильтр тегов постов для вывода на этой странице. пусто = все
      aPostCategories:[] # фильтр категорий постов для вывода на этой странице. пусто = все
    }
    {
      sSlug: "news"
      sTitle: 'Новости'
      sBrowserTitle: 'Новости'
      bIsSinglePage: false
      aPostTags:[] # фильтр категорий постов для вывода на этой странице
      aPostCategories: [
        {id:"category-news", sTitle: "Новости"}
      ]
    }
    {
      sSlug: "about"
      sTitle: 'О сайте'
      sBrowserTitle: 'О сайте'
      bIsSinglePage: true
      sPageId: "page-about"
      aPostTags:[]
      aPostCategories: []
    }
    {
      sSlug: "contacts"
      sTitle: 'Контакты'
      sBrowserTitle: 'Контакты'
      bIsSinglePage: true
      sPageId: "page-contacts"
      aPostTags:[]
      aPostCategories: []
    }
  ]

