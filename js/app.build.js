({
    baseUrl:'.',
    include: ['app','require-js'],
    stubModules: ['text'],
    out: "app.js",
    paths: {
        'require-js'       : 'libs/require-js/require',
        'jquery'           : 'libs/jquery/jquery-1.9.1',
        'jquery-plugins'   : 'libs/jquery/jquery.plugins',
        'jquery-history'   : 'libs/jquery/plugins/jquery-history',
        'jvm-settings'     : 'libs/dataview/jvm-settings',
        'jvm-console'      : './libs/dataview/jvm-console',
        'jvm-extensions'   : 'libs/dataview/jvm-extensions',

        'stylesheets-pack' : '../css/stylesheets.pack',
        'lib-pack'         : 'libs/lib.pack',
        'domReady'         : 'libs/require-js/plugins/domReady',
        'text'             : 'libs/require-js/plugins/text',
        'image'            : 'libs/require-js/plugins/image',

        'underscore'       : 'libs/underscore'
    },
    shim: {
        'jquery-plugins'                 : ["jquery"],
        'libs/dataview/jvm-helpers'       : ['jquery'],
        'libs/bootstrap/js/bootstrap.min' : ['jquery'],

        'settings'                             : ['jvm-settings'],

        'jvm-console'           : ['underscore','jvm-extensions'],
        'jvm-extensions'        : ['jquery','underscore'],
        'lib-pack'              : ['jvm-console'],
        'stylesheets-pack'      : ['jvm-extensions']

    },

    preserveLicenseComments : false,
    logLevel:0
})