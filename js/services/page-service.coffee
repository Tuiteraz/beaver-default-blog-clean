define [
  'jvm-console'
  'libs/dataview/jvm-templates'
  'helpers/helpers'
  'templates/common-tmpl'
], (jconsole,tmpl,hlprs,common_tmpl) ->

  jconsole.info "page-service_tmpl."

  # +2013.7.16 tuiteraz
  before_filter: ->
    @aPages ||= []

  # отправить заспро на сервер для получения страницы
  # +2013.7.16 tuiteraz
  get: (hQuery) ->
    jconsole.warn "pages-service: get() [stub]"
    # -( stubs
    hPage = _.where STUBS.PAGES, hQuery
    # -) stubs

    return hPage

  # +2013.7.19 tuiteraz
  get_and_push: (hQuery) ->
    Res = @get hQuery # отправим запрос на сервер
    if _.isArray Res
      if _.size(Res) == 1
        @aPages.push Res[0]
      else
        @aPages.push hPage for hPage in Res
    else
      @aPages.push Res

    return Res

  # Query(string) = id
  # Query(Object) = {id,sSlug ...}
  # +2013.7.16 tuiteraz
  find: (Query) ->
    @before_filter()

    if typeof Query == 'string'
      hQuery = {id:Query}
    else
      hQuery = Query

    Res = []
    Res = _.where(@aPages, Query) if _.size(@aPages) > 0   # check local storage
    Res = @get_and_push(hQuery) if _.isEmpty Res           # get from server and push into local

    if _.size(Res) == 1
      return Res[0]
    else if _.size(Res) > 0
      return Res
    else
      return null

  # Page(string) = id
  # Page(hash) = hPage{}
  # +2013.7.17 tuiteraz
  render: (Page) ->

    if typeof(Page) =="string"
      hPage = @find Page # probably it's page id
    else if _.isObject(page)
      hPage = Page

    jContainer = $("##{SITE.content_container_id}")
    jContainer.fadeOut 'fast', ->
      $(this).empty()
      $(this).append common_tmpl.render_page hPage
      $(this).fadeIn('fast')
