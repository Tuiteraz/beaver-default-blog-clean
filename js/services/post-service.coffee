define [
  'jvm-console'
  'libs/dataview/jvm-templates'
  'helpers/helpers'
  'templates/common-tmpl'
], (jconsole,tmpl,hlprs,common_tmpl) ->

  jconsole.info "post-service"

  # +2013.7.18 tuiteraz
  before_filter: ->
    @aPosts ||= []

  # check hPost with local storage
  # +2013.7.19 tuiteraz
  is_new : (hPost) ->
    Res = _.where @aPosts, {id: hPost.id}
    return _.isEmpty(Res)

  # отправить заспро на сервер для получения поста
  # +2013.7.18 tuiteraz
  # *2013.7.19 tuiteraz: +sMode
  get: (hQuery, hParams=null) ->
    jconsole.warn "post-service: get() [stub]"
    # -( stubs
    hPost = _.filter STUBS.POSTS, (hItem)=>
      @filter(hItem,hQuery)

    sMode = SITE.post.get_modes.data

    if defined hParams
      sMode = hParams.sMode if defined hParams.sMode

    if sMode == SITE.post.get_modes.length
      Res = _.size hPost
    else
      Res = hPost
    # -) stubs

    return Res

  # +2013.7.19 tuiteraz
  get_and_push: (hQuery) ->

    Res = @get hQuery # отправим запрос на сервер
    if _.isArray Res
      if _.size(Res) == 1
        @aPosts.push Res[0] if @is_new Res[0]
      else
        for hPost in Res
          @aPosts.push hPost if @is_new hPost
    else
      @aPosts.push Res if is_new Res

    return Res

  # +2013.7.19 tuiteraz
  get_selection_size: (hQuery) ->
    return @get(hQuery, {sMode : SITE.post.get_modes.length} )

  # using for _.filter
  # +2013.7.18 tuiteraz
  filter: (hItem,hQuery) ->
    aKeys = _.keys hQuery
    bRes = true
    for sKey in aKeys
      if !_.isObject hQuery[sKey]
        bRes = false if hItem[sKey] != hQuery[sKey]
      else if !_.isEmpty hQuery[sKey]
        bRes = false if !_.isEqual hItem[sKey], hQuery[sKey]

      break if !bRes

    #jconsole.log hItem.id if bRes

    return bRes

  # Query(string) = id
  # Query(Object) = {id,sSlug ...}
  # +2013.7.18 tuiteraz
  find: (Query) ->
    @before_filter()

    if typeof Query == 'string'
      hQuery = {id:Query}
    else
      hQuery = Query

    if _.size(@aPosts) > 0
      Res = _.filter @aPosts, (hItem)=>
        @filter(hItem,hQuery)

      # обратить к серверу за данными если: локальная выборка пуста ИЛИ её размер меньше чем серверной
      Res = @get_and_push(hQuery) if _.isEmpty(Res) || ( _.size(Res) < @get_selection_size(hQuery) )
    else
      Res = @get_and_push hQuery # отправим запрос на сервер

    if _.size(Res) == 1
      return Res[0]
    else if _.size(Res) > 0
      return Res
    else
      return null

  # single post render
  # +2013.7.18 tuiteraz
  render: (Post)->
    if typeof(Post) =="string"
      hPost = @find Post # probably it's page id
    else if _.isObject(Post)
      hPost = Post

    jContainer = $("##{SITE.content_container_id}")
    jContainer.fadeOut 'fast', ->
      $(this).empty().append common_tmpl.render_post_scaffolding()
      $(this).contents().contents(".post-container").append common_tmpl.render_post hPost
      $(this).contents().contents(".sidebar").append common_tmpl.render_sidebar()
      $(this).fadeIn 'fast'

  # list post render
  # hQuery = {aTags[string], aCategories[{id,sTitle}]}
  # +2013.7.18 tuiteraz
  render_by: (hQuery) ->
    aSelection = @find hQuery

    jContainer = $("##{SITE.content_container_id}")
    jContainer.fadeOut 'fast', ->
      jContainer.empty().append common_tmpl.render_post_scaffolding()

      if _.size(aSelection) > 0
        for hPost in aSelection
          jContainer.contents().contents(".post-container").append common_tmpl.render_post hPost, SITE.post.modes.exerpt

      jContainer.contents().contents(".sidebar").append common_tmpl.render_sidebar()
      jContainer.fadeIn 'fast'
