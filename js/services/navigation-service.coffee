define [
  'jvm-console'
  'libs/dataview/jvm-templates'
  'helpers/helpers'
  'templates/common-tmpl'
  './page-service'
  './post-service'
], (jconsole,tmpl,hlprs,common_tmpl,page,post) ->

  jconsole.info "navigation-service_tmpl."

  # security data check when needed
  # +2013.7.16 tuiteraz
  before_filter: ->
    @get if !defined @aNavItems


  # +2013.7.16 tuiteraz
  init: ->
    @get()
    @select_by_url()

  # +2013.7.19 tuieraz
  bind_events: ->
    me = this


    $(window).bind "statechange", (e) =>
      @select_by_url()

      if !@is_empty()
        @update_nav_items_class()
        if @is_single_page()
          @event_on_single_page_click()
        else if @is_single_post()
          @event_on_single_post_click()
        else if @is_posts_list()
          @event_on_posts_list_click()



    $("a[data-nav-slug]").unbind("click").click (e) ->
      sNavSlug = $(this).attr 'data-nav-slug'
      send_event_to me, {sNavSlug: sNavSlug, sAction: SITE.actions.goto_nav_link}
      e.preventDefault()

    $(document).delegate "a[data-post-id]","click", (e) ->
      sCurrNavSlug = $(document).getUrlParam SITE.url_param_names.nav
      sPostId = $(this).attr 'data-post-id'
      send_event_to me, {sNavSlug: sCurrNavSlug, sPostId: sPostId, sAction: SITE.actions.goto_post_link}
      e.preventDefault()

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        if hDetails.sAction == SITE.actions.goto_nav_link
          @goto hDetails.sNavSlug

        else if hDetails.sAction == SITE.actions.goto_post_link
          hUrlParams = {}
          hUrlParams[SITE.url_param_names.post] = hDetails.sPostId
          @goto hDetails.sNavSlug, hUrlParams


  # +2013.7.18 tuiteraz
  event_on_single_page_click:  ->
    @before_filter()

    sPageId = @get_current_page_id()
    page.render sPageId

  # +2013.7.18 tuiteraz
  event_on_single_post_click:  ->
    @before_filter()

    sPostId = @get_current_post_id()
    post.render sPostId

  # +2013.7.18 tuiteraz
  event_on_posts_list_click:  ->
    @before_filter()

    hQuery = @get_posts_query()
    post.render_by hQuery

  # +2013.7.30 tuiteraz
  get_current_title: ->
    return @hCurrNavItem.sTitle

  # если в параметрах указан элемент меню к которму прикреплена страница,
  # то вернем ее id
  # +2013.7.17 tuiteraz
  get_current_page_id: ->
    @before_filter()

    return @hCurrNavItem.sPageId

  # +2013.7.18 tuiteraz
  get_current_post_id: ->
    @before_filter()
    sPostId = $(document).getUrlParam SITE.url_param_names.post
    return sPostId

  # если в параметрах указан элемент меню к которму прикреплена страница,
  # то вернем ее slug
  # +2013.7.16 tuiteraz
  get_current_page_slug: ->
    @before_filter()
    hPage = page.find @hCurrNavItem.sPageId
    return hPage.sSlug

  # +2013.7.16 tuiteraz
  get: ->
    jconsole.warn "navigation-service: get [stub]"
    # -( stub
    @aNavItems = STUBS.NAVIGATION
    # -) stub

    # ajax call to juicy-mongo needed


  # собрать из параметров пункта меню запрос для получения постов
  # +2013.7.18 tuiteraz
  get_posts_query: ->
    @before_filter()

    {
      aTags : @hCurrNavItem.aPostTags
      aCategories : @hCurrNavItem.aPostCategories
    }

  # +2013.7.19 tuiteaz
  # *2013.7.19 tuiteaz: hUrlParams
  goto: (sNavSlug, hUrlParams=null) ->
    @before_filter()

    hNavItem = _.detect @aNavItems, (hItem)->
      hItem.sSlug == sNavSlug

    if defined hNavItem
      @hCurrNavItem = hNavItem
    else if _.size(@aNavItems) > 0
      @hCurrNavItem = @aNavItems[0]

    sUrlParams = ""
    if defined hUrlParams
      aKeys = _.keys hUrlParams
      for sKey in aKeys
        sUrlParams += "&#{sKey}=#{hUrlParams[sKey]}"

    sTitle = "#{SITE.sBrowserMainTitle} - #{@hCurrNavItem.sTitle}"
    History = window.History
    History.pushState(null, sTitle , "?nav=#{@hCurrNavItem.sSlug}#{sUrlParams}")


  # выполнить имитацию перехода на главную страницу - когда первый раз загружаем сайт
  # после этого все функции работы с текущим пунктом меню будут выдавать индексную страницу
  # +2013.7.16 tuiteraz
  goto_index: ->
    @before_filter()

    hNavItem = _.detect @aNavItems, (hItem) ->
      hItem.bIsIndex == true

    if defined hNavItem
      @hCurrNavItem = hNavItem
    else if _.size(@aNavItems) > 0
      @hCurrNavItem = @aNavItems[0]

    sTitle = "#{SITE.sBrowserMainTitle} - #{@hCurrNavItem.sTitle}"
    History = window.History
    History.pushState(null, sTitle, "?nav=#{@hCurrNavItem.sSlug}")

  # когда первый раз сайт открывает и в параметрах ничего нет
  # или не настроено меню
  # +2013.7.16 tuiteraz
  is_empty: ->
    @before_filter()

    if _.size(@aNavItems) > 0
      return false
    else
      return true

  # для пункта меню с фильтром категорий или тегов = true
  # +2013.7.16 tuiteraz
  is_posts_list: ->
    @before_filter()

    bRes = false if @is_single_page() || @is_single_post()
    bRes ||= true
    #jconsole.info "is_posts_list = #{bRes}"

    return bRes

  # для пункта меню с прикрепленной статической страницей = true
  # +2013.7.16 tuiteraz
  is_single_page: ->
    @before_filter()
    bRes = @hCurrNavItem.bIsSinglePage
    #jconsole.info "is_single_page = #{bRes}"

    return bRes

  # для любого выбранного пункта меню может быть открыть отдельно пост
  # только если это не пункт привязанный к странице
  # +2013.7.16 tuiteraz
  is_single_post: ->
    @before_filter()

    bRes = true
    if !@is_single_page()
      sPostId = $(document).getUrlParam SITE.url_param_names.post
      bRes = false if !defined sPostId
    else
      bRes = false

    #jconsole.info "is_single_post = #{bRes}"

    return bRes

  # +2013.7.17 tuiteraz
  render: ->
    @before_filter()

    sLiHtml = ""
    for hNavItem in @aNavItems
      sHref = "?#{SITE.url_param_names.nav}=#{hNavItem.sSlug}"
      sParams ="data-nav-slug='#{hNavItem.sSlug}'"
      sIcon = ""
      sIcon = tmpl.twbp_icon(hNavItem.sIcon) if defined hNavItem.sIcon

      sLiHtml += tmpl.li "", [
        tmpl.a sHref, sParams, sIcon + hNavItem.sTitle
      ]

    sHtml = tmpl.div "navbar", [
      tmpl.div "navbar-inner", [
        tmpl.ul "nav", [sLiHtml]
      ]
    ]
    $("##{SITE.nav_container_id}").empty().append sHtml


  # выбираем пункт меню по параметрам в урле
  # +2013.7.16 tuiteraz
  select_by_url: ->
    sNavSlug = $(document).getUrlParam SITE.url_param_names.nav
    if !defined sNavSlug
      @goto_index()
    else
      @hCurrNavItem = _.detect @aNavItems, (hNavItem) ->
        hNavItem.sSlug == sNavSlug

  # +2013.7.19 tuiteraz
  update_nav_items_class: ->
    sCurrNavSlug = @hCurrNavItem.sSlug
    $("a[data-nav-slug!='#{sCurrNavSlug}']").parent().removeClass 'active'
    $("a[data-nav-slug='#{sCurrNavSlug}']").parent().addClass 'active'
