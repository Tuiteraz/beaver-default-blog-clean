define [
  'libs/dataview/jvm-templates'
  'helpers/helpers'
  'jvm-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "common-tmpl"

  # +2013.7.17 tuiteraz
  render_scaffolding: ->
    $('head').append @render_google_counter()
    $('body').empty().append @render_yandex_counter()
    $('body').append @render_container()
    $("##{SITE.site_title.container_id}").empty().append @render_site_title()
    $("##{SITE.top_slide_container_id}").empty().append @render_top_slider()
    $("##{SITE.footer.container_id}").empty().append @render_footer()

  # +2013.8.22 tuiteraz
  render_google_counter: ->
    """
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-43416135-1', 'wtrmln.org');
    ga('send', 'pageview');

    </script>
    """

  # +2013.8.22 tuiteraz
  render_yandex_counter: ->
    """
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter22134065 = new Ya.Metrika({id:22134065, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/22134065" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    """

  # +2013.7.16 tuiteraz
  render_container: ->
    tmpl.div "container", [
      tmpl.div "row-fluid", SITE.site_title.container_id , []
      tmpl.div "row-fluid", SITE.top_slide_container_id , []
      tmpl.div "row-fluid", SITE.nav_container_id ,[]
      tmpl.div "row-fluid", SITE.content_container_id ,[]
      tmpl.div "row-fluid", SITE.footer.container_id ,[]
    ]

  # +2013.7.17 tuiteraz
  render_page: (hPage) ->
    tmpl.div "span12 page", [
      tmpl.div "title", [
        tmpl.h 1, hPage.sTitle
      ]
      tmpl.div "content", [
        hPage.sContent
      ]

    ]

  # +2013.7.18 tuiteraz
  render_post: (hPost, sMode=SITE.post.modes.full) ->
    if defined hPost
      if sMode == SITE.post.modes.full
        iHeading = 1
        sExtraClass = ""
        sContentHml = hPost.sContent

      else if sMode == SITE.post.modes.exerpt
        sNavParam = $(document).getUrlParam SITE.url_param_names.nav
        sHref = "?#{SITE.url_param_names.nav}=#{sNavParam}&#{SITE.url_param_names.post}=#{hPost.id}"
        iHeading = 3
        sExtraClass = "post-exerpt"
        sContentHml = hPost.sContent.substring 0,SITE.post.modes.iExerptLength
        sContentHml += "...<br>"

        sParams = "data-post-id='#{hPost.id}'"
        sContentHml += tmpl.a sHref,sParams,"Читать дальше","read-more"

      sPostContent = tmpl.div "post #{sExtraClass}", [
        tmpl.div "title", [
          tmpl.h iHeading, hPost.sTitle
        ]
        tmpl.div "content", [
          sContentHml
        ]
      ]
    else
      sPostContent = tmpl.div "post", [
        tmpl.div "title", [
          tmpl.h 1, "error 404"
          tmpl.span "", "Post not found"
        ]
      ]

    return sPostContent

  # +2013.7.18 tuiteraz
  render_post_scaffolding: ->
    tmpl.div "span12", [
      tmpl.div "span9 post-container", []
      tmpl.div "span3 sidebar", []
    ]


  # +2013.7.18 tuiteraz
  render_sidebar: ->
    tmpl.div "", [
      tmpl.html_tag "aside","", [
        tmpl.h 3, "НЕДАВНИЕ ЗАПИСИ"
        tmpl.ul "", [
          tmpl.li "", [
            tmpl.a "#","","NEWS POST 001"
          ]
          tmpl.li "", [
            tmpl.a "#","","NEWS POST 002"
          ]
        ]
      ]
      tmpl.html_tag "aside","", [
        tmpl.h 3, "АРХИВЫ"
        tmpl.ul "", [
          tmpl.li "", [
            tmpl.a "#","","Июль 2013"
          ]
          tmpl.li "", [
            tmpl.a "#","","Июнь 2013"
          ]
          tmpl.li "", [
            tmpl.a "#","","Май 2013"
          ]
        ]
      ]
      tmpl.html_tag "aside","", [
        tmpl.h 3, "КАТЕГОРИИ"
        tmpl.ul "", [
          tmpl.li "", [
            tmpl.a "#","","Новости"
          ]
          tmpl.li "", [
            tmpl.a "#","","Блог"
          ]
        ]
      ]
    ]

  render_site_title: ->
    tmpl.div "span12", [
      tmpl.h 1, SITE.site_title.sTitle, "site-title"
      tmpl.span "site-sub-title",[SITE.site_title.sSubTitle]
    ]

  render_top_slider: ->
    iImgCount = _.size SITE.slider.aImages
    sCurrImgFleName = SITE.slider.aImages[Math.RandomInteger(iImgCount)-1]
    tmpl.img "#{SITE.slider.sImagesBasePath}/#{sCurrImgFleName}"

  render_footer: ->
    tmpl.div "span12", [
      tmpl.span "credits", [SITE.footer.sCredits]
    ]

